

const animationData = [
    {
        ingridient: "lime",
        img: "assets/img/lime.png",
        intoGlass: "assets/img/glass_lime.png",
        description: "Lima con azúcar"
    },
    {
        ingridient: "mint",
        img: "assets/img/mint.png",
        intoGlass: "assets/img/glass_mint.png",
        description: "Hojas de menta"
    },
    {
        ingridient: "bacardi",
        img: "assets/img/bacardi.png",
        intoGlass: "assets/img/glass_bacardi.png",
        description: "Bacardi"
    },
    {
        ingridient: "ice",
        img: "assets/img/ice.png",
        intoGlass: "assets/img/glass_ice.png",
        description: "Hielo picado"
    },  
    {
        ingridient: "soda",
        img: "assets/img/soda.png",
        intoGlass: "assets/img/glass_soda.png",
        description: "Soda"
    }
    
]

//info 
let info = document.querySelector('.info');
let infoP = document.querySelector('.info-p');



//Animations container

const animationContainer = document.querySelector('.animation-container');
const fillGlass = document.querySelector('.fill-glass');
let ingridientsContainer = document.querySelector('.ingridients');
let glass = document.querySelector('.glass');
const initialGlass = "assets/img/empty_glass.png";
const divCocktail = document.createElement('div');
const btnContainer = document.querySelector('.btn-container');


// Ingridients to add

let imgIngridient = document.createElement('img');
imgIngridient.draggable = true;
let nameIngridient = document.createElement('figcaption');
imgIngridient.classList.add("add-ingridient");
let i = 0;

//glass to change
let imgGlass = document.createElement('img');
imgGlass.setAttribute('src', initialGlass);
glass.appendChild(imgGlass);





const showIngridientsMobile = () => {

    ingridient=animationData[i];
    
    imgIngridient.setAttribute('src', ingridient.img);
    nameIngridient.innerHTML = ingridient.description;
    ingridientsContainer.appendChild(imgIngridient);
    ingridientsContainer.appendChild(nameIngridient);

    onTouchDisplay();
}

function onTouchDisplay () {

    imgIngridient.addEventListener('touchstart', touchStartHandler);
    imgIngridient.addEventListener('touchmove', touchMovetHandler);
    imgIngridient.addEventListener('touchend', touchEndtHandler);

}

let touchStartHandler = (e) => {
    this.className += ' in_drag';
    setTimeout(() => {
      this.className = 'invisible';
    }, 0);
  };
  
  let touchMovetHandler = (e) => {
    e.preventDefault();

  };
  
  let touchEndtHandler = (e) => {
      if (i === 4 ) {
          bacardiCocktailDone();
        } else if (i<4){
        this.className = 'add-ingridient';
        this.className = 'glass';
        imgGlass.setAttribute('src', ingridient.intoGlass);
        glass.appendChild(imgGlass);
    
        anotherIngridient();
    }
  };


const showIngridients = () => {

    ingridient=animationData[i];
    
    imgIngridient.setAttribute('src', ingridient.img);
    nameIngridient.innerHTML = ingridient.description;
    ingridientsContainer.appendChild(imgIngridient);
    ingridientsContainer.appendChild(nameIngridient);

    imgIngridient.addEventListener('dragstart', dragStart);
    imgIngridient.addEventListener('dragend', dragEnd);

    glass.addEventListener('dragover', dragOver);
    glass.addEventListener('dragleave', dragLeave);
    glass.addEventListener('dragenter', dragEnter);
    glass.addEventListener('drop', dragDrop);

}



function dragStart() {
    this.className += ' in_drag';
    setTimeout(() => {
      this.className = 'invisible';
    }, 0);
}


function dragEnd() {
    this.className = 'add-ingridient';
}
   

function dragOver(e) {
    e.preventDefault();
  }
  
  function dragEnter(e) {
    e.preventDefault();
    this.className += ' hover';
  }
  
  function dragLeave(e) {
    this.className = 'glass';
  }
  
  function dragDrop(e) {
    
    if (i === 4 ) {
        bacardiCocktailDone();
    } else if (i<4){
        this.className = 'glass';
        imgGlass.setAttribute('src', ingridient.intoGlass);
        glass.appendChild(imgGlass);
    
        anotherIngridient();
    }
  }




function anotherIngridient() {
    i++;
   ingridient = animationData[i];
    imgIngridient.setAttribute('src', ingridient.img);
    nameIngridient.innerHTML = ingridient.description;
}

function bacardiCocktailDone() {
    infoP.innerHTML="¡ENHORABUENA!";
    animationContainer.innerHTML = '';
    info.style.height= "unset";
    yourBacardi();

}

function yourBacardi() {
    const backgroundLight = document.createElement('div');
    backgroundLight.classList.add('background-light');
    const bacardiCocktail = document.createElement('img');
    bacardiCocktail.setAttribute('src', animationData[4].intoGlass);
    divCocktail.classList.add('cocktail');

    const btn$$ = document.createElement('button');
    btn$$.innerHTML = 'VEN A POR TU MOJITO BACARDI';
    btn$$.classList.add = 'btn';
    backgroundLight.appendChild(bacardiCocktail);
    divCocktail.appendChild(backgroundLight);
    animationContainer.appendChild(divCocktail);
    btnContainer.appendChild(btn$$);
    btn$$.addEventListener("click", ()=> {
        window.location.replace('https://www.bacardi.com/')
    });

    setTimeout(()=>{shine()}, 2500);

}

function shine() {
    const star1 = document.createElement('div');
    const star2 = document.createElement('div');

    star1.classList.add('star-one');
    star2.classList.add('star-two');

    divCocktail.appendChild(star1);
    divCocktail.appendChild(star2);
}


window.onload= () => {

    if(window.PointerEvent) {
        showIngridients();
    }
        showIngridientsMobile();
    
    
}